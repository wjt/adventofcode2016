import itertools
from os import path

input_text = path.join(path.dirname(path.abspath(__file__)), 'input.txt')

def is_triangle(sides):
    a, b, c = sorted(sides)
    return a + b > c


def read_input():
    with open(input_text) as f:
        for line in f:
            yield list(map(int, line.split()))


def part_one():
    print(sum(1 for sides in read_input() if is_triangle(sides)))


# got this right first time, booyah
def blocks(g, size):
    yield from zip(*[
        itertools.islice(g_, i, None, size)
        for i, g_ in enumerate(itertools.tee(g, size))
    ])


def transpose(xss):
    return zip(*xss)


def part_two():
    n = 0

    for block in blocks(read_input(), 3):
        for triple in transpose(block):
            n += is_triangle(triple)

    print(n)


if __name__ == '__main__':
    part_two()
