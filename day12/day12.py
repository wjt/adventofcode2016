import collections
import enum
import functools
from os import path
import re

input_text = path.join(path.dirname(path.abspath(__file__)), 'input.txt')
test_text = path.join(path.dirname(path.abspath(__file__)), 'test.txt')


class Machine(object):
    def __init__(self, filename, c=0):
        self.r = {x: 0 for x in "abd"}
        self.r['c'] = c
        self.pc = 0
        self.instructions = []
        with open(filename, 'r') as f:
            for line in f:
                inst, *args = line.split()
                try:
                    c = getattr(self, 'compile_' + inst)
                except AttributeError:
                    f = functools.partial(getattr(self, inst), *args)
                else:
                    f = c(*args)
                self.instructions.append(f)

    def compile_cpy(self, x, y):
        if x.isdigit():
            return functools.partial(self.cpy_i, int(x), y)
        else:
            return functools.partial(self.cpy_r, x, y)

    def compile_jnz(self, x, y):
        if x.isdigit():
            return functools.partial(self.jnz_i, int(x), int(y))
        else:
            return functools.partial(self.jnz_r, x, int(y))

    def cpy_i(self, x, y):
        self.r[y] = x
        self.pc += 1

    def cpy_r(self, x, y):
        self.cpy_i(self.r[x], y)

    def inc(self, x):
        self.r[x] += 1
        self.pc += 1

    def dec(self, x):
        self.r[x] -= 1
        self.pc += 1

    def jnz_i(self, x, y):
        if x:
            self.pc += y
        else:
            self.pc += 1

    def jnz_r(self, x, y):
        self.jnz_i(self.r[x], y)

    def run(self):
        n = len(self.instructions)
        while 0 <= self.pc < n:
            f = self.instructions[self.pc]
            f()


if __name__ == '__main__':
    m = Machine(input_text, c=1)
    m.run()
    print(m.r)
