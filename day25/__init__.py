#!/usr/bin/env python3
import argparse
import itertools


def compile_arg(arg):
    try:
        return int(arg)
    except ValueError:
        return arg


class Found(Exception):
    pass
class NotFound(Exception):
    pass


class Machine(object):
    def __init__(self, filename):
        self.instructions = []
        with open(filename, 'r') as f:
            for line in f:
                inst, *args = line.split('#')[0].split()
                f = (getattr(Machine, inst), *(compile_arg(a) for a in args))
                self.instructions.append(f)

        self.reset()

    def reset(self):
        self.r = {x: 0 for x in "abcd"}
        self.pc = 0
        self.output = []
        self.seen_states = {}  # (a, b, c, d, pc) -> output

    def val(self, x):
        if isinstance(x, str):
            return self.r[x]
        elif isinstance(x, int):
            return x
        else:
            raise ValueError(x)

    def cpy(self, x, y):
        x_ = self.val(x)
        if y in self.r:
            self.r[y] = x_
        self.pc += 1

    def inc(self, x):
        self.r[x] += 1
        self.pc += 1

    def dec(self, x):
        self.r[x] -= 1
        self.pc += 1

    def jnz(self, x, y):
        x_ = self.val(x)
        if x_:
            y_ = self.val(y)
            self.pc += y_
        else:
            self.pc += 1

    def add(self, x, y):
        if y in self.r:
            self.r[y] += self.val(x)
        self.pc += 1

    def out(self, x):
        v = self.val(x)
        self.output.append(v)
        self.pc += 1

    def mul(self, x, y):
        if y in self.r:
            self.r[y] *= self.val(x)
        self.pc += 1

    def nop(self):
        print('nop')
        self.pc += 1

    @classmethod
    def _list_one(cls, i, f, args):
        print(('%3d %s' + (' %3s' * len(args))) % (i, f.__name__, *args))

    @classmethod
    def _list(cls, instructions):
        for i, (f, *args) in enumerate(instructions):
            cls._list_one(i, f, args)

    @profile
    def _push_state(self):
        state = (self.r["a"], self.r["b"], self.r["c"], self.r["d"], self.pc)
        if state in self.seen_states:
            print("loop", self.seen_states[state], "->", self.output)
            if all(o == i % 2 for i, o in enumerate(self.output)):
                raise Found
            else:
                raise NotFound
        else:
            self.seen_states[state] = self.output.copy()

    @profile
    def run(self):
        n = len(self.instructions)
        while 0 <= self.pc < n:
            self._push_state()
            f, *args = self.instructions[self.pc]
            f(self, *args)


def main():
    p = argparse.ArgumentParser()
    p.add_argument('source')
    a = p.parse_args()
    m = Machine(a.source)
    for i in itertools.count():
        print(i)
        m.reset()
        m.r['a'] = i
        try:
            m.run()
        except Found:
            print("hooray", i)
            break
        except NotFound:
            print("nope", i)

if __name__ == '__main__':
    main()
