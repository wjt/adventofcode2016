#!/usr/bin/env python3
# I am really bad at number theory, but the internet is much more informative
# than it was when I was studying it…
import re
import functools
import operator

test_input = '''
Disc #1 has 5 positions; at time=0, it is at position 4.
Disc #2 has 2 positions; at time=0, it is at position 1.
'''.strip().split('\n')

puzzle_input = '''
Disc #1 has 13 positions; at time=0, it is at position 11.
Disc #2 has 5 positions; at time=0, it is at position 0.
Disc #3 has 17 positions; at time=0, it is at position 11.
Disc #4 has 3 positions; at time=0, it is at position 0.
Disc #5 has 7 positions; at time=0, it is at position 2.
Disc #6 has 19 positions; at time=0, it is at position 17.
'''.strip().split('\n')

line_rx = re.compile(r'Disc #(\d+) has (\d+) positions; at time=0, it is at position (\d+).')


def eqns(lines):
    return [
        disc(*map(int, m.groups()))
        for m in map(line_rx.match, lines)
    ]


def disc(k, n, p):
    print('Disc #{k} has {n} positions; at time=0, it is at position {p}.'.format(**locals()))
    print('need ((t + {k}) + {p}) = 0 mod {n}'.format(**locals()))
    # t + k + p = 0 mod n
    # <=>
    # t = -(p + k) mod n
    q = (-(p + k)) % n
    print('t = {q} mod {n}'.format(**locals()))
    return (q, n)


def crt(qns):
    '''
    http://rosettacode.org/wiki/Chinese_remainder_theorem
    '''
    qs, ns = zip(*qns)
    N = functools.reduce(operator.mul, ns)
    x = 0
    for q, n in zip(qs, ns):
        N_ = N // n
        r, s = eea(n, N_)
        x += q * s * N_

    return x % N


def eea(a, b):
    '''
    https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
    '''
    q = []
    r = [a, b]
    s = [1, 0]
    t = [0, 1]

    while r[-1] != 0:
        q_, r_ = divmod(r[-2], r[-1])
        q.append(q_)
        r.append(r_)
        s.append(s[-2] - q[-1] * s[-1])
        t.append(t[-2] - q[-1] * t[-1])

    gcd = r[-2]
    x = s[-2]
    y = t[-2]
    assert x * a + y * b == gcd, locals()
    return x, y


if __name__ == '__main__':
    qns = eqns(puzzle_input)
    print(crt(qns))
    qns.append(disc(len(qns) + 1, 11, 0))
    print(crt(qns))
