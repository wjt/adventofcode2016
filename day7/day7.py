import collections
import itertools
import re
from os import path

input_text = path.join(path.dirname(path.abspath(__file__)), 'input.txt')
test_text = path.join(path.dirname(path.abspath(__file__)), 'test.txt')

def read_input(filename):
    with open(filename) as f:
        for line in f:
            yield line.strip()


brackets = re.compile(r'\[|\]')
abba = re.compile(r'(.)([^\1])\2\1')


def chunks(s, n):
    for i in range(len(s) - n + 1):
        yield s[i:i+n]


def supports_tls(ip):
    in_brackets = False
    tls = False

    for chunk in chunks(ip, 4):
        c, d, e, f = chunk
        if c == '[':
            in_brackets = True
        elif c == ']':
            in_brackets = False
        else:
            if c == f != d == e:
                if in_brackets:
                    tls = False
                    break
                else:
                    tls = True

    print(ip, '=>', tls)
    return tls


def supports_ssl(ip):
    in_brackets = False
    exterior_abs = set()
    interior_abs = set()

    for chunk in chunks(ip, 3):
        c, d, e = chunk
        if c == '[':
            in_brackets = True
        elif c == ']':
            in_brackets = False
        else:
            if c == e != d:
                if in_brackets:
                    interior_abs.add((d, c))
                else:
                    exterior_abs.add((c, d))

    ssl = bool(exterior_abs & interior_abs)
    print(ip, '=>', ssl)
    return ssl


if __name__ == '__main__':
    print(sum(map(supports_ssl, read_input(input_text))))
