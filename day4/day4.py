import itertools
import collections
import re
from os import path

input_text = path.join(path.dirname(path.abspath(__file__)), 'input.txt')

def read_input():
    with open(input_text) as f:
        for line in f:
            yield parse(line.strip())


room_rx = re.compile(r'^([a-z\-]*)-(\d+)\[([a-z]+)\]$')
Room = collections.namedtuple('Room', 'name sector checksum')


def parse(s):
    m = room_rx.match(s)
    name = m.group(1)
    sector = m.group(2)
    checksum = m.group(3)
    return Room(name, int(sector), checksum)


def is_valid(room):
    c = collections.Counter(room.name)
    del c['-']
    checksum = ''.join(itertools.islice(
        (
            x
            for n, g in itertools.groupby(c.most_common(), lambda t: t[1])
            for x, n_ in sorted(g)
        ),
        5
    ))
    return checksum == room.checksum


def shift(c, n):
    return chr(((ord(c) - ord('a')) + n) % 26 + ord('a'))


def decrypt(room):
    return ''.join(
        shift(c, room.sector) if c != '-' else ' '
        for c in room.name
    )


def part1():
    print(sum(room.sector for room in read_input() if is_valid(room)))


def part2():
    for room in read_input():
        if is_valid(room):
            print(room, decrypt(room))


if __name__ == '__main__':
    part2()
