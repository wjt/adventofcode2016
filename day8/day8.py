import numpy as np
import re
from os import path

input_text = path.join(path.dirname(path.abspath(__file__)), 'input.txt')
test_text = path.join(path.dirname(path.abspath(__file__)), 'test.txt')


def read_input(filename):
    with open(filename) as f:
        for line in f:
            yield line.strip()


rect_rx = re.compile(r'rect (\d+)x(\d+)')
rotate_rx = re.compile(r'rotate (row y|column x)=(\d+) by (\d+)')


def rect(screen, m):
    cols, rows = map(int, m.groups())
    screen[:rows, :cols] = True


def rotate(screen, m):
    axis, a, shift = m.groups()
    a = int(a)
    shift = int(shift)
    if axis == 'row y':
        screen[a, :] = np.roll(screen[a, :], shift)
    else:
        screen[:, a] = np.roll(screen[:, a], shift)


def apply_line(screen, line):
    for rx, f in ((rect_rx, rect), (rotate_rx, rotate)):
        m = rx.match(line)
        if m is not None:
            f(screen, m)
            break
    else:
        raise ValueError(line)


def display(screen):
    for i in range(screen.shape[0]):
        print(''.join('■' if c else ' ' for c in screen[i]))
    print()


def apply_lines(screen, lines):
    display(screen)
    for line in lines:
        apply_line(screen, line)
        display(screen)


def test_part1():
    lines = [
        'rect 3x2',
        'rotate column x=1 by 1',
        'rotate row y=0 by 4',
        'rotate column x=1 by 1',
    ]
    apply_lines(np.zeros((3, 7), dtype=np.bool_), lines)


def part1():
    screen = np.zeros((6, 50), dtype=np.bool_)
    apply_lines(screen, read_input(input_text))
    print(screen.sum())


if __name__ == '__main__':
    part1()
