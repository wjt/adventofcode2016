#!/usr/bin/env python3
"""Hooray, more graph search, I think.

Here the states are not positions on the grid, they are paths. This is because
the legal moves depend on the path so far.
"""

from collections import namedtuple, deque, OrderedDict
from hashlib import md5
from heapq import heappush, heappop, heapify
from pprint import pprint


DIRS = [
    (b'U', 0, -1),
    (b'D', 0, 1),
    (b'L', -1, 0),
    (b'R', 1, 0),
]


def neighbours(path, x, y):
    prefix = md5(path).hexdigest()
    ns = []
    for i in (0, 1, 2, 3):
        c = prefix[i]
        if 'b' <= c <= 'f':
            (d, dx, dy) = DIRS[i]
            n_x = x + dx
            n_y = y + dy
            if 0 <= n_x <= 3 and 0 <= n_y <= 3:
                n_path = path + d
                n = (n_path, n_x, n_y)
                ns.append(n)

    return ns


def search(seed):
    q = deque([(seed, 0, 0)])

    while q:
        path, x, y = q.popleft()
        # print("considering", u)

        if x == 3 and y == 3:
            yield path[len(seed):]
        else:
            q.extend(neighbours(path, x, y))


def part1(seed):
    '''Shortest path'''
    for result in search(seed):
        return result


def part2(seed):
    '''Longest path'''
    return max(search(seed), key=len)


def main():
    examples = [
        (b'ihgpwlah', b'DDRRRD', 370),
        (b'kglvqrro', b'DDUDRLRRUDRD', 492),
        (b'ulqzkmiv', b'DRURDRUDDLLDLUURRDULRLDUUDDDRR', 830),
    ]
    for seed, expected, max_len in examples:
        r = part1(seed)
        print('***', seed, r)
        assert r == expected
        print('max is', max_len)
        l = part2(seed)
        assert len(l) == max_len

    print(part1(b'pxxbnzuo'))
    print(len(part2(b'pxxbnzuo')))


if __name__ == '__main__':
    main()
