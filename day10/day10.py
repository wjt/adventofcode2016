import collections
import enum
from os import path
import re

input_text = path.join(path.dirname(path.abspath(__file__)), 'input.txt')
test_text = path.join(path.dirname(path.abspath(__file__)), 'test.txt')


class Type_(enum.Enum):
    bot = 1
    output = 2


Dest = collections.namedtuple('Dest', ('type_ ix'))
Rule = collections.namedtuple('Rule', ('lo hi'))

init_rx = re.compile(r'value (\d+) goes to bot (\d+)')
rule_rx = re.compile(r'bot (\d+) gives low to (bot|output) (\d+) and high to (bot|output) (\d+)')


class C(object):
    def __init__(self):
        self.outputs = {}
        self.rules = {}
        self.state = {}
        self.compared = {}
        self.ready = set()

    def add_rule(self, rule):
        m = init_rx.match(rule)
        if m:
            v, b = map(int, m.groups())
            self.give(b, v)
            return

        m = rule_rx.match(rule)
        if m:
            b, lo_t, lo_ix, hi_t, hi_ix = m.groups()
            self.rules[int(b)] = Rule(
                Dest(Type_[lo_t], int(lo_ix)),
                Dest(Type_[hi_t], int(hi_ix)))
            return

        raise ValueError(rule)

    def give(self, ix, v):
        print(ix, 'gets', v)
        vs = self.state.setdefault(ix, set())
        vs.add(v)
        if len(vs) == 2:
            print(ix, 'is ready')
            self.ready.add(ix)

    def run(self):
        while self.ready:
            ix = self.ready.pop()
            rule = self.rules[ix]
            vs = self.state.pop(ix)
            print('despatching', ix, rule, vs)
            lo, hi = sorted(vs)
            self.compared[(lo, hi)] = ix
            self.deliver(rule.lo, lo)
            self.deliver(rule.hi, hi)

    def deliver(self, dest, v):
        if dest.type_ == Type_.output:
            self.outputs[dest.ix] = v
        else:
            self.give(dest.ix, v)

if __name__ == '__main__':
    c = C()
    with open(input_text) as f:
        for line in f:
            c.add_rule(line)

    c.run()
    print(c.state)
    print(c.outputs)
    print(c.compared)
    print(c.compared.get((17, 61)))
    print(c.outputs[0] * c.outputs[1] * c.outputs[2])
