#!/usr/bin/env python3
from heapq import heappush, heappop
from pprint import pprint


def manhattan(u, v):
    (ux, uy) = u
    (vx, vy) = v
    return abs(ux - ux) + abs(uy - vy)


def is_wall(z, puzzle_input):
    x, y = z

    a = x*x + 3*x + 2*x*y + y + y*y + puzzle_input

    ret = bin(a).count('1') % 2 == 1
    print(' ???', z, ret)
    return ret


dirs = (
    (1, 0),
    (-1, 0),
    (0, 1),
    (0, -1),
)


def neighbours(u):
    ux, uy = u
    for dx, dy in dirs:
        vx, vy = (ux + dx, uy + dy)
        if vx >= 0 and vy >= 0:
            yield (vx, vy)


def main(puzzle_input, goal, reachable_in=None):
    start = (1, 1)

    closed = set()

    # open set
    q = []  # heap[[f(u), u], ...]]
    q_by_coord = {}  # u -> [f(u), u]

    g = {start: 0}

    came_from = {}

    def h(u):
        return manhattan(u, goal)

    def f(u):
        return g[u] + h(u)

    def push(u):
        assert u not in q_by_coord
        q_entry = [f(u), u]
        heappush(q, q_entry)
        q_by_coord[u] = q_entry

    def reconstruct_path(v):
        path = [v]
        while v in came_from:
            v = came_from[v]
            path.insert(0, v)
        return path

    push(start)

    while q:
        f_u, u = heappop(q)
        del q_by_coord[u]
        print("considering", u)

        if u in closed:
            continue

        closed.add(u)

        if reachable_in is None and u == goal:
            return reconstruct_path(u)

        reheap = False
        for v in neighbours(u):
            if is_wall(v, puzzle_input):
                continue

            if v in closed:
                continue

            print("  neighbour", v)

            g_v = g[u] + 1
            if reachable_in is not None and g_v > reachable_in:
                continue

            if v not in g:
                # new discovery
                g[v] = g_v
                came_from[v] = u
                push(v)
            elif g_v >= g[v]:
                # not a better path
                continue
            else:
                g[v] = g_v
                came_from[v] = u
                q_entry = q_by_coord[v]
                assert q_entry[1] == v
                q_entry[0] = f(v)
                reheap = True

        if reheap:
            heapify(q)

    if reachable_in is not None:
        return g


if __name__ == '__main__':
    test_number = 10
    test_goal = (7, 4)

    puzzle_input = 1350
    puzzle_goal = (31, 39)

    if False:
        # part 1
        path = main(puzzle_input, puzzle_goal)

        pprint(path)
        print(len(path) - 1)
    else:
        # part 2
        g = main(puzzle_input, puzzle_goal, 50)
        print(len(g))

