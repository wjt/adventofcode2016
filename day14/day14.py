#!/usr/bin/env python3
import hashlib
import itertools
import re
import sys

triplet_rx = re.compile(r'(.)\1\1')

def stream(salt, reps=1):
    for i in itertools.count():
        h = '{}{}'.format(salt, i)
        # Amazingly the encode() is about 25% of the cost of this loop, and the _ in range() is
        # about 15%!
        for _ in range(reps):
            h = hashlib.md5(h.encode('ascii')).hexdigest()
        m = triplet_rx.search(h)
        c = m.group(1) if m is not None else None
        yield i, h, c


def keys(s):
    buf = list(itertools.islice(s, 1001))
    while True:
        i, h, c = buf.pop(0)
        if c is not None:
            ccccc = c * 5
            # print('got', c, 'at', i, 'seeking', ccccc)
            if any(ccccc in h_ for (_, h_, _) in buf):
                yield i, h, c
        buf.append(next(s))


def main():
    test_salt = 'abc'
    puzzle_salt = 'ngcjuoqr'
    keystream = keys(stream(test_salt, reps=2017))
    for j, (i, h, c) in itertools.islice(enumerate(keystream, 1), 64):
        print(j, i, h, c)


if __name__ == '__main__':
    main()
