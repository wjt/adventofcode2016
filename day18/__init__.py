#!/usr/bin/env python3

def successor(row):
    return tuple(
        l != r
        for l, r in zip((False,) + row[:-1], row[1:] + (False,))
    )


def parse(string):
    return tuple(c == '^' for c in string)


def pp(row):
    return ''.join('^' if c else '.' for c in row)


def main(string, n):
    row = parse(string)
    # print(pp(row))
    traps = len(row) - sum(row)
    for _ in range(n - 1):
        row = successor(row)
        # print(pp(row))
        traps += len(row) - sum(row)
    print(traps)


if __name__ == '__main__':
    main('..^^.', 3)
    print()
    main('.^^.^.^^^^', 10)
    print()
    main('^^^^......^...^..^....^^^.^^^.^.^^^^^^..^...^^...^^^.^^....^..^^^.^.^^...^.^...^^.^^^.^^^^.^^.^..^.^', 40)
    print()
    main('^^^^......^...^..^....^^^.^^^.^.^^^^^^..^...^^...^^^.^^....^..^^^.^.^^...^.^...^^.^^^.^^^^.^^.^..^.^', 400000)
