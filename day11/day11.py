#!/usr/bin/env python3
import collections
import enum
import itertools
from os import path
import re
import sys


class Kind(enum.Enum):
    microchip = 1
    generator = 2


class State(collections.namedtuple('State', 'elevator floors key')):
    __slots__ = ()

    def __new__(cls, elevator, floors):
        key = {}
        for i, floor in enumerate(floors):
            for kind, m in floor:
                key.setdefault(m, [None, None])[kind.value - 1] = i

        key = (elevator, tuple(sorted(tuple(v) for v in key.values())))
        return super(State, cls).__new__(cls, elevator, floors, key)

    @classmethod
    def parse(cls, filename):
        floors = []

        with open(filename, 'r') as f:
            for line in f:
                floor = set()

                for m in re.findall(r'(\w+) generator', line):
                    floor.add((Kind.generator, m))

                for m in re.findall(r'(\w+)-compatible microchip', line):
                    floor.add((Kind.microchip, m))

                floors.append(frozenset(floor))

        return cls(0, floors)

    def is_legal(self):
        for floor in self.floors:
            c = {m for kind, m in floor if kind == Kind.microchip}
            g = {m for kind, m in floor if kind == Kind.generator}
            unshielded = c - g
            for m in unshielded:
                if g - {m}:
                    return False
        return True

    def is_terminal(self):
        return all(len(floor) == 0 for floor in self.floors[:-1])

    def successors(self):
        e = self.elevator
        floor = self.floors[e]

        for d in (-1, +1):
            e_ = e + d
            if e_ < 0 or e_ >= len(self.floors):
                continue

            cargoes = itertools.chain(
                ({c} for c in floor),
                itertools.combinations(floor, 2),
            )
            for cargo in cargoes:
                cargo = set(cargo)
                floors_ = self.floors.copy()
                floors_[e] = floors_[e] - cargo
                floors_[e_] = floors_[e_] | cargo
                succ = State(elevator=e_, floors=floors_)
                if succ.is_legal():
                    yield succ

    def __str__(self):
        ret = []
        for i, floor in enumerate(self.floors):
            ret.append(' '.join((
                str(i),
                'E' if i == self.elevator else '.',
                ' '.join(m[0] + k.name[0] for k, m in floor))))

        return '\n'.join(reversed(ret))


def bfs(state):
    queue = [(0, state, [])]
    seen = set()
    while queue:
        dist, s, path = queue.pop(0)
        if s.is_terminal():
            for s_ in path:
                print(s_)
                print()
            print(s)
            return dist

        for succ in s.successors():
            # TODO: prune
            if not succ.key in seen:
                seen.add(succ.key)
                queue.append((dist + 1, succ, path + [s]))


if __name__ == '__main__':
    s = State.parse(sys.argv[1])
    print(bfs(s))
