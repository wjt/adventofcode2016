import argparse


def compile_arg(arg):
    try:
        return int(arg)
    except ValueError:
        return arg


class OptimizedInstructionError(Exception):
    pass


class Machine(object):
    def __init__(self, filename):
        self.r = {x: 0 for x in "abcd"}
        self.r['a'] = 12
        self.pc = 0
        self.instructions = []
        self.no_jmp_plz = set()
        with open(filename, 'r') as f:
            for i, line in enumerate(f):
                inst, *args = line.split('#')[0].split()
                if args[-1] == '|':
                    self.no_jmp_plz.add(i)
                    args.pop()
                f = (getattr(Machine, inst), *(compile_arg(a) for a in args))
                self.instructions.append(f)

    def val(self, x):
        if isinstance(x, str):
            return self.r[x]
        elif isinstance(x, int):
            return x
        else:
            raise ValueError(x)

    def cpy(self, x, y):
        x_ = self.val(x)
        if y in self.r:
            self.r[y] = x_
        self.pc += 1

    def inc(self, x):
        self.r[x] += 1
        self.pc += 1

    def dec(self, x):
        self.r[x] -= 1
        self.pc += 1

    def jnz(self, x, y):
        x_ = self.val(x)
        if x_:
            y_ = self.val(y)
            self.pc += y_
            if self.pc in self.no_jmp_plz:
                raise OptimizedInstructionError
        else:
            self.pc += 1

    def tgl(self, x):
        i = self.pc + self.r[x]
        if i in self.no_jmp_plz:
            raise OptimizedInstructionError
        if 0 <= i < len(self.instructions):
            f, *args = self.instructions[i]
            g = getattr(f, 'toggles_to')
            self.instructions[i] = (g, *args)
        self.pc += 1

    def add(self, x, y):
        if y in self.r:
            self.r[y] += self.val(x)
        self.pc += 1  # replaces inc/dec/jnz

    def mul(self, x, y):
        if y in self.r:
            self.r[y] *= self.val(x)
        self.pc += 1

    def nop(self):
        print('nop')
        self.pc += 1

    @classmethod
    def _list_one(cls, i, f, args):
        print(('%3d %s' + (' %3s' * len(args))) % (i, f.__name__, *args))

    @classmethod
    def _list(cls, instructions):
        for i, (f, *args) in enumerate(instructions):
            cls._list_one(i, f, args)

    # unary
    inc.toggles_to = dec
    dec.toggles_to = inc
    tgl.toggles_to = inc
    # binary
    jnz.toggles_to = cpy
    cpy.toggles_to = jnz

    def run(self, debug=False):
        n = len(self.instructions)
        c = 'n'
        while 0 <= self.pc < n and c != 'q':
            f, *args = self.instructions[self.pc]
            self._list_one(self.pc, f, args)
            if not debug:
                f(self, *args)
            else:
                print('> ', end='')
                c = input()
                if c == 'n':
                    f(self, *args)
                elif c == 'l':
                    self._list(self.instructions)
                elif c == 'p':
                    print(self.r)


def main():
    p = argparse.ArgumentParser()
    p.add_argument('--debug', action='store_true')
    p.add_argument('source')
    a = p.parse_args()
    m = Machine(a.source)
    m.run(debug=a.debug)
    print(m.r)

if __name__ == '__main__':
    main()
