import collections
import itertools
from os import path

input_text = path.join(path.dirname(path.abspath(__file__)), 'input.txt')
test_text = path.join(path.dirname(path.abspath(__file__)), 'test.txt')

def read_input(filename):
    with open(filename) as f:
        for line in f:
            yield line.strip()


def nth_common(filename, n=0):
    counters = []
    for line in read_input(filename):
        if not counters:
            counters = list(map(collections.Counter, line))
        else:
            for counter, char in zip(counters, line):
                counter[char] += 1

    return ''.join(
        counter.most_common()[n][0]
        for counter in counters
    )


if __name__ == '__main__':
    print(nth_common(input_text, n=-1))
