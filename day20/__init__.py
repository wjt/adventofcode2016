#!/usr/bin/env python3
import sys


def parse(filename):
    with open(filename, 'r') as f:
        blacklist = [
            tuple(map(int, line.strip().split('-')))
            for line in f
        ]
    blacklist.sort(key=lambda t: (t[0], -t[1]))
    return blacklist


def part1(blacklist):
    i = 0
    for lo, hi in blacklist:
        print('[{}, {}]'.format(lo, hi))
        if i < lo:
            return i
        elif i <= hi:
            i = hi + 1
    return i


def part2(blacklist):
    allowed = 0
    i = 0
    for lo, hi in blacklist:
        print('[{}, {}]'.format(lo, hi))
        if i < lo:
            allowed += (lo - i)

        if i <= hi:
            i = hi + 1
    allowed += (2 ** 32) - i
    return allowed



if __name__ == '__main__':
    bl = parse(sys.argv[1])
    print(part1(bl))
    print(part2(bl))
