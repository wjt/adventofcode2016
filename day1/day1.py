#!/usr/bin/env python3
import numpy as np

L = np.array(((0, -1), (1, 0)))
R = L.T

rotations = {
    'L': L,
    'R': R,
}


def parse_step(step):
    print(step)
    return rotations[step[0]], int(step[1:])


def parse_steps(steps):
    yield from map(parse_step, steps.split(', '))


def manhattan_distance(pos):
    return abs(pos[0]) + abs(pos[1])


def final_stop(steps):
    pos = np.array((0, 0))
    v = np.array((0, 1))

    print(pos)

    for r, distance in parse_steps(steps):
        v = r @ v
        pos += (distance * v)
        print(pos)

    return manhattan_distance(pos)


def first_repetition(steps):
    pos = np.array((0, 0))
    v = np.array((0, 1))
    visited = set(tuple(pos))

    print(pos)

    for r, distance in parse_steps(steps):
        print(visited)
        v = r @ v
        for _ in range(distance):
            pos += v
            print(pos)

            if tuple(pos) in visited:
                return manhattan_distance(pos)

            visited.add(tuple(pos))

    return None


puzzle_input = 'L4, L1, R4, R1, R1, L3, R5, L5, L2, L3, R2, R1, L4, R5, R4, L2, R1, R3, L5, R1, L3, L2, R5, L4, L5, R1, R2, L1, R5, L3, R2, R2, L1, R5, R2, L1, L1, R2, L1, R1, L2, L2, R4, R3, R2, L3, L188, L3, R2, R54, R1, R1, L2, L4, L3, L2, R3, L1, L1, R3, R5, L1, R5, L1, L1, R2, R4, R4, L5, L4, L1, R2, R4, R5, L2, L3, R5, L5, R1, R5, L2, R4, L2, L1, R4, R3, R4, L4, R3, L4, R78, R2, L3, R188, R2, R3, L2, R2, R3, R1, R5, R1, L1, L1, R4, R2, R1, R5, L1, R4, L4, R2, R5, L2, L5, R4, L3, L2, R1, R1, L5, L4, R1, L5, L1, L5, L1, L4, L3, L5, R4, R5, R2, L5, R5, R5, R4, R2, L1, L2, R3, R5, R5, R5, L2, L1, R4, R3, R1, L4, L2, L3, R2, L3, L5, L2, L2, L1, L2, R5, L2, L2, L3, L1, R1, L4, R2, L4, R3, R5, R3, R4, R1, R5, L3, L5, L5, L3, L2, L1, R3, L4, R3, R2, L1, R3, R1, L2, R4, L3, L3, L3, L1, L2'

def main():
    print(first_repetition(puzzle_input))


if __name__ == '__main__':
    main()
