import hashlib
import itertools
import sys


def five_zeroes(door_id):
    for i in itertools.count():
        h = hashlib.md5(b'%s%d' % (door_id, i)).hexdigest()
        if h[:5] == '00000':sh ge
            # print(i, h[5])
            yield h


def part_one(door_id):
    g = (h[5] for h in five_zeroes(door_id))
    print(''.join(itertools.islice(g, 8)))


def part_two(door_id):
    n = 8
    password = ['_'] * n
    for h in five_zeroes(door_id):
        if not h[5].isdigit():
            continue

        i = int(h[5])
        c = h[6]
        if 0 <= i < n and password[i] == '_':
            password[i] = c

        print(''.join(password))
        if all(x != '_' for x in password):
            break


puzzle_input = b'ffykfhsq'


if __name__ == '__main__':
    part_two(puzzle_input)
