import re
from os import path

input_text = path.join(path.dirname(path.abspath(__file__)), 'input.txt')
x_by_y = re.compile(r'\((\d+)x(\d+)\)')


def go(s, recurse):
    i = 0
    length = 0

    m = x_by_y.search(s, pos=i)
    while m:
        length += (m.start() - i)
        x, y = map(int, m.groups())
        i = m.end() + x
        if recurse:
            k = go(s[m.end():i], True)
        else:
            k = x
        length += (k * y)

        m = x_by_y.search(s, pos=i)

    length += len(s) - i
    return length


def v1(s):
    return go(s, False)


def v2(s):
    return go(s, True)


def main():
    s = open(input_text).read().strip()
    print(v2(s))


if __name__ == '__main__':
    main()
