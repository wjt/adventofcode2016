#!/usr/bin/env python
# pylint: disable=C0111,C0103
'''http://adventofcode.com/2016/day/16'''
import numpy as np


ZERO = np.array((0,), dtype=np.bool_)


def pprint(label, buffer):
    print(label, ''.join(['1' if x else '0' for x in buffer]))


def parse(seed):
    return np.array([x == '1' for x in seed], dtype=np.bool_)


def extend(buffer, size):
    # pprint('initial', buffer)
    while len(buffer) < size:
        buffer = np.concatenate((buffer, ZERO, ~buffer[::-1]))
        # pprint('extended to', buffer)

    return buffer[:size]


def checksum(buffer):
    # pprint('checksum start', buffer)
    while True:
        buffer = ~(buffer[:-1:2] ^ buffer[1::2])
        # pprint('reduced to', buffer)
        if len(buffer) % 2 == 1:
            return buffer


def main(seed, size):
    buffer = parse(seed)
    pprint('result', checksum(extend(buffer, size)))


if __name__ == '__main__':
    main("10111011111001111", 35651584)
