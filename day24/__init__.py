#!/usr/bin/env python3
import argparse
import collections
import numpy as np
import pulp


Problem = collections.namedtuple('Problem', 'grid digits')
Graph = collections.namedtuple('Graph', 'nodes edges')

def parse(f):
    grid = np.array([list(line.strip()) for line in f])
    digits = {}
    for i, line in enumerate(grid):
        for j, cell in enumerate(line):
            if cell.isdigit():
                digits[(i, j)] = int(cell)
    print(grid.shape)
    return Problem(grid, digits)


def bfs(grid, origin, goals):
    to_visit = collections.deque([(origin, 0)])
    done = grid == '#'
    done[origin] = True
    best = {}
    m, n = grid.shape
    while to_visit:
        ij, cost = to_visit.popleft()

        if ij in goals:
            best[(origin, ij)] = cost
            best[(ij, origin)] = cost

        i, j = ij
        for i_, j_ in ((i-1, j), (i+1, j), (i, j-1), (i, j+1)):
            if 0 <= i_ < m and 0 <= j_ < n and not done[i_, j_]:
                done[i_, j_] = True
                to_visit.append(((i_, j_), cost + 1))

    return best


def solve(graph, loop=False):
    problem = pulp.LpProblem()

    n = len(graph.nodes)
    assert max(graph.nodes) + 1 == n, graph.nodes

    xijs = np.zeros(shape=(n, n), dtype=np.dtype(pulp.LpVariable))
    for i in graph.nodes:
        for j in graph.nodes:
            xijs[i, j] = pulp.LpVariable("x_{}_{}".format(i, j),
                                         cat='Binary')

    if not loop:
        problem.addConstraint(sum(xijs[:, 0]) == 0, "no path to zero")

    for i in graph.nodes:
        if loop or i != 0:
            problem.addConstraint(sum(
                xijs[j, i]
                for j in graph.nodes
                if j != i
            ) == 1, "exactly one path to node {i}".format(i=i))

        problem.addConstraint(sum(
            xijs[i, j]
            for j in graph.nodes
            if j != i
        ) <= 1, "at most one path from node {i}".format(i=i))

    # Exactly n-1 or n paths
    expected_paths = n if loop else n - 1
    problem.addConstraint(xijs.sum() == expected_paths,
                          "exactly {} paths".format(expected_paths))

    # All subtours pass through 0
    us = [(i, pulp.LpVariable("u_{}".format(i))) for i in graph.nodes if i != 0]
    for i, u_i in us:
        for j, u_j in us:
            if i != j:
                problem.addConstraint(u_i - u_j + (n * xijs[i, j]) <= n - 1,
                                      "no non-trivial subtours {}, {}".format(i, j))

    problem.sense = pulp.constants.LpMinimize
    problem.objective = sum(
        graph.edges[i, j] * xijs[i, j]
        for i in graph.nodes
        for j in graph.nodes
    )

    print(problem)
    status = problem.solve(pulp.solvers.GLPK(msg=0))
    print(pulp.LpStatus[status])
    print((np.vectorize(pulp.value)(xijs) * graph.edges).sum())
    for i in graph.nodes:
        print(xijs[i, :])
        print(list(map(pulp.value, xijs[i, :])))


def main():
    p = argparse.ArgumentParser()
    p.add_argument('grid', type=argparse.FileType())
    p.add_argument('--loop', action='store_true')
    a = p.parse_args()
    problem = parse(a.grid)

    edges = {}
    xys = list(problem.digits)
    for i, xy in enumerate(xys):
        edges.update(bfs(problem.grid, xy, xys[i:]))

    costs = np.zeros(shape=[len(problem.digits)] * 2, dtype=np.dtype(int))
    print(costs)
    for (p, q), cost in edges.items():
        costs[problem.digits[p], problem.digits[q]] = cost
    graph = Graph(nodes=set(problem.digits.values()),
                  edges=costs)
    print(graph.edges)

    solve(graph, loop=a.loop)

if __name__ == '__main__':
    main()
