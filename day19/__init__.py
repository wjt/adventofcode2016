#!/usr/bin/env python3
from collections import deque


def part1(n):
    q = deque(range(1, n+1))
    while q:
        a = q.popleft()
        try:
            q.popleft()
        except IndexError:
            return a
        else:
            q.append(a)



def part2(n):
    elves = [True for i in range(n)]
    i = 0
    j = n // 2
    live = n

    def advance(k, m):
        for _ in range(m):
            while True:
                k = (k + 1) % n
                if elves[k]:
                    break
        return k

    while live > 1:
        assert elves[i]
        assert elves[j]
        assert i != j

        # sigil = {i: '*', j: 'x'}
        # print(' '.join(
        #     '{}{}'.format(sigil.get(k, ' '), k + 1)
        #     for k, state in enumerate(elves)
        #     if state
        # ))

        elves[j] = False
        live -= 1

        i = advance(i, 1)
        j = advance(j, 1 if live % 2 else 2)

    return elves.index(True) + 1

if __name__ == '__main__':
    print(part1(3014387))
    print(part2(3014387))
