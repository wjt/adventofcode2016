import collections
import os
import re

import numpy as np


dt = np.dtype([('size', np.int16), ('used', np.int16)])

rows = 30
cols = 34


def load():
    p = re.compile(r'''
        /dev/grid/node-x(\d+)-y(\d+)
        \s+
        (\d+)T
        \s+
        (\d+)T
        \s+
        (\d+)T
        \s+
        (\d+)%
        $''', flags=re.X)
    nodes = np.zeros((cols, rows), dtype=dt)
    with open(os.path.join(os.path.dirname(__file__), 'input.txt')) as f:
        next(f)
        next(f)
        for line in f:
            m = p.match(line.strip())
            if not m:
                raise ValueError(line)
            x, y, size, used, _avail, _use = map(int, m.groups())
            assert size - used == _avail
            nodes[x, y] = (size, used)
    return nodes


def part1(nodes):
    viable = 0

    for ix, a in np.ndenumerate(nodes):
        if a['used'] != 0:
            q = a['used'] + nodes['used']
            candidates = q < nodes['size']
            q[ix] = False
            viable += candidates.sum()

    assert viable == 993


def part2(nodes):
    print(nodes[:, 0])
    print(nodes[:, 0]['size'])
    print(nodes[:, 0]['size'] - nodes[-1, 0]['used'])



def main():
    nodes = load()
    part1(nodes)
    part2(nodes)


if __name__ == '__main__':
    main()
