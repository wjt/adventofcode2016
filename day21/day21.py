#!/usr/bin/env python3

import argparse
import functools
import re

import numpy as np


def index(a, x):
    return np.argwhere(a == x)[0][0]


def swap_position(x, y, a, invert=False):
    r'swap position (\d+) with position (\d+)'
    _symmetrical = invert
    i = int(x)
    j = int(y)
    a[i], a[j] = a[j], a[i]
    return a


def swap_letter(x, y, a, invert=False):
    r'swap letter (\w) with letter (\w)'
    i = index(a, x)
    j = index(a, y)
    return swap_position(i, j, a, invert=invert)


def rotate_steps(direction, x, a, invert=False):
    r'rotate (left|right) (\d+) steps?'
    i = int(x)
    if (direction == 'left') is (not invert):
        j = -i
    else:
        j = i
    return np.roll(a, j)


def rotate_pos_indices(n):
    ixs = [None] * n
    for i in range(n):
        j = 1 + i + (1 if i >= 4 else 0)
        assert ixs[(i + j) % n] is None, (i, ixs)
        ixs[(i + j) % n] = j
    return ixs


def rotate_pos(x, a, invert=False):
    r'rotate based on position of letter (\w+)'
    i = index(a, x)
    if invert:
        ixs = rotate_pos_indices(len(a))
        j = ixs[i]
        print('rotate left by', j)
        ret = rotate_steps('left', j, a)
        assert all(rotate_pos(x, ret) == a)
        return ret
    else:
        j = 1 + i + (1 if i >= 4 else 0)
        return rotate_steps('right', j, a)


def reverse(x, y, a, invert=False):
    r'reverse positions (\d+) through (\d+)'
    _symmetrical = invert
    i = int(x)
    j = int(y)
    a[i:j+1] = a[i:j+1][::-1]
    return a


def move(x, y, a, invert=False):
    r'move position (\d+) to position (\d+)'
    i = int(x)
    j = int(y)
    if invert:
        j, i = i, j
    b = a[i]
    if i < j:
        a[i:j] = a[i+1:j+1]
    else:
        a[j+1:i+1] = a[j:i]
    a[j] = b
    return a


operations = [
    (re.compile(func.__doc__), func)
    for func in [
        swap_position,
        swap_letter,
        rotate_steps,
        rotate_pos,
        reverse,
        move
    ]
]


def parse(path):
    ops = []
    with open(path, 'r') as f:
        for line in f:
            line = line.strip()
            for rx, func in operations:
                m = rx.match(line)
                if m:
                    ops.append((line, functools.partial(func, *m.groups())))
                    break
            else:
                raise ValueError(line)
    return ops


def main():
    p = argparse.ArgumentParser()
    p.add_argument('--invert', action='store_true')
    p.add_argument('path')
    p.add_argument('seed')
    args = p.parse_args()

    ops = parse(args.path)
    if args.invert:
        ops.reverse()
    a = np.array(list(args.seed))

    import ipdb
    with ipdb.launch_ipdb_on_exception():
        for line, op in ops:
            print(''.join(a))
            print(line)
            a = op(a, invert=args.invert)

    print(''.join(a))


if __name__ == '__main__':
    main()
